import { BaseSessionManager } from "./baseSessionManager"
import { DefaultSessionData } from "./defaultSessionData"

export class DefaultSessionManager extends BaseSessionManager {
  constructor(container) {
    super(container)
  }

  async get(authCreds) {
    // this.log.info(`SessionManager get: ${JSON.stringify(authCreds)}`)
    return new DefaultSessionData(this.container)
  }

  /**
   * Squirl away session data for the specified auth credentials
   * @param {*} authCreds
   * @param {*} sessionData
   */
  async set(authCreds, sessionData) {
    return true
  }
}
