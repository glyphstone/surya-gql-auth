import { BaseSessionData } from "./baseSessionData"

/**
 *
 */
export class BaseSessionManager {
  constructor(container) {
    this.container = container
    this.log = container.log
  }

  /**
   * If the authCreds correspond to a valid user, return current session Data for that user
   * that can be used to authentiate and authorize user actions
   * @param {*} authCreds - may contain an token or basic credential
   */
  async get(authCreds) {
    return new BaseSessionData(this.container)
  }

  /**
   * Squirl away session data for the specified auth credentials
   * @param {*} authCreds
   * @param {*} sessionData
   */
  async set(authCreds, sessionData) {
    return true
  }
}
