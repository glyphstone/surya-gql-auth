export class BaseSessionData {
  constructor(container) {
    this.log = container.log
  }
  async getUserId() {
    return ""
  }

  async getAccountId() {
    return ""
  }

  async isInGroup(groupName) {
    return true
  }
}
