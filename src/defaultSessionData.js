import { BaseSessionData } from "./baseSessionData"

export class DefaultSessionData extends BaseSessionData {
  async getUserId() {
    return "5bef2b6a9f74c2f7df49868a"
  }

  async getAccountId() {
    return "5bef2b6a9f74c2f7df49868b"
  }

  async isInGroup(groupName) {
    //this.log.info(`isInGroup(${groupName})`)
    return true
  }
}
